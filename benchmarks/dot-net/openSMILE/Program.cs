﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using openSMILETests;

namespace openSMILE
{
    class Program
    {
        static void Main(string[] args)
        {
            var inPath = @"D:\Projects\audio-analysis-dev\benchmarks\input\Round2_SessionId_46.wav";
            var outPath = @"D:\Projects\audio-analysis-dev\benchmarks\dot-net\output\LLD.csv";

            var cmd = $"-C ComParE_LLD.conf -nologfile -I {inPath} -instname {inPath} -csvoutput {outPath} -l 1";
            var charCmd = cmd.ToCharArray();

            var test = OpenSmileAdapter.test(3);
            Console.WriteLine(test);

            var retCode = OpenSmileAdapter.main(charCmd.Length, charCmd);

            Console.WriteLine(retCode);

            Console.ReadKey();
        }
    }
}
