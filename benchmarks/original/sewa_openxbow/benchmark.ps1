# Active directory must be this file's location

$count = 10

$result = (Measure-Command {
    1..$count | % {
        java -jar openXBOW.jar -i ..\..\input\LLD.csv -o ..\output\BAOW.libsvm -a 10 -norm 1 -b codebook
    }
}).TotalMilliseconds

$output = "$(Get-Date) Original openXBOW: Runs: $count, Average: $($result / $count) MS Total: $result MS."

Write-Host $output

Add-Content "..\..\results.log" $output

# Write-Host "Press any key to continue ..."