# Active directory must be this file's location

$input_wav = "..\..\input\Round2_SessionId_46.wav"
$output_file = "..\output\LLD.csv"
$count = 10

$result = (Measure-Command {
    1..$count | % {
        & .\SMILExtract_Release.exe -C ComParE_LLD.conf -nologfile -I $input_wav -instname $input_wav -csvoutput $output_file -l 1
    }
}).TotalMilliseconds

$output = "$(Get-Date) Original openSMILE: Runs: $count, Average: $($result / $count) MS Total: $result MS."

Write-Host $output

Add-Content "..\..\results.log" $output

# Write-Host "Press any key to continue ..."