#!/usr/bin/env bash
source activate sewa
python sewa_openxbow.py -i "../test_data/Round2_SessionId_46/LLD.csv" \
    -o "../test_data/Round2_SessionId_46/BOAW.libsvm"
source deactivate sewa
