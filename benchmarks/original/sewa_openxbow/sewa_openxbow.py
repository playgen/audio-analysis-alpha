import os
import sys
import getopt


def main(argv):
    input_file = None
    output_file = None
    opts, args = getopt.getopt(argv, 'i:o:', ['ifile=', 'ofile='])
    for opt, arg in opts:
        if opt in ("-i", "--ifile"):
            input_file = arg
        elif opt in ("-o", "--ofile"):
            output_file = arg
    if input_file is None:
        print 'Please specify the input CSV file path!'
    elif output_file is None:
        print 'Please specify the output LIBSVM file path!'
    else:
        print 'Input CSV file path: ' + input_file
        print 'Output LIBSVM file path: ' + output_file
        input_file = os.path.realpath(input_file)
        output_file = os.path.realpath(output_file)
        command = 'java -jar openXBOW.jar -i "' + input_file + \
                  '" -o "' + output_file + '" -a 10 -norm 1 -b codebook'
        os.chdir(os.path.dirname(os.path.realpath(__file__)))
        os.system(command)
        print 'LIBSVM features computed.'


if __name__ == "__main__":
    main(sys.argv[1:])
