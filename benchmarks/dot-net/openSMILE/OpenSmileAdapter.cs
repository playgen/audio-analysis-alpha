﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace openSMILETests
{
    public static class OpenSmileAdapter
    {
        /*
         * DllImport defaults to CallingConvention.WinApi, 
         * which is identical to CallingConvention.StdCall 
         * for x86 desktop code. It should be 
         * CallingConvention.Cdecl
         */
        [DllImport("SMILExtract_Debug.dll", EntryPoint = @"?test@@YAHH@Z", CallingConvention = CallingConvention.Cdecl)]
        public static extern int test(int argc);

        [DllImport("SMILExtract_Debug.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int main(int argc, char[] argv);
    }
}
