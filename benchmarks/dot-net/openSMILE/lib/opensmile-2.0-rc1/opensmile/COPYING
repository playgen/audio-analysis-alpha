
  openSMILE Version 2.0
   - open Speech and Music Interpretation by Large-space Extraction -
  Copyright (C) 2013, Institute for Human-Machine Communication
  Authors: Florian Eyben, Felix Weninger, Martin Woellmer, Bjoern Schuller
  
  Institute for Human-Machine Communication
  Technische Universitaet Muenchen (TUM)
  D-80333 Munich, Germany

 ********************************************************************** 
 If you use openSMILE or any code from openSMILE in your research work,
 you are kindly asked to acknowledge the use of openSMILE in your publications.
 See the file CITING.txt for details.
 **********************************************************************


This TUM Research License Agreement (license, license agreement, or agreement in the ongoing), is a legal agreement between you and Technische Universitaet Muenchen (TUM or we) for the software or data identified above, which may include source code, and any associated materials, text or speech files, associated media and "online" or electronic documentation (together, the "Software").  

By installing, copying, or otherwise using this Software, you agree to be bound by the terms in this license.  If you do not agree, then do not install copy or use the Software. The Software is protected by copyright and other intellectual property laws and is licensed, not sold.

**This license grants you the following rights:**
You may use, copy, reproduce, and distribute this Software for any non-commercial purpose, subject to the restrictions set out below. Some purposes which can be non-commercial are teaching, academic research, public demonstrations and personal experimentation or personal home use. You may also distribute this Software with books or other teaching materials, or publish the Software on websites, that are intended to teach the use of the Software for academic or other non-commercial purposes. 
You may NOT use or distribute this Software or any derivative works in any form for commercial purposes. Examples of commercial purposes would be running business operations, licensing, leasing, or selling the Software, distributing the Software for use with commercial products (no matter whether free or paid), using the Software in the creation or use of commercial products or any other activity which purpose is to procure a commercial gain to you or others.
If the Software includes source code or data, you may create derivative works of such portions of the Software and distribute the modified Software for non-commercial purposes, as provided herein.  
If you distribute the Software or any derivative works of the Software, you will distribute them under the same terms and conditions as in this license, and you will not grant other rights to the Software or derivative works that are different from those provided by this license agreement. 
If you have created derivative works of the Software, and distribute such derivative works, you will cause the modified files to carry prominent notices so that recipients know that they are not receiving the original Software. Such notices must state: (i) that you have changed the Software; and (ii) the date of any changes.

**In return, you agree to:** 
1. That you will not remove any copyright or other notices (authors and citing information, for example) from the Software.
2. That if any of the Software is in binary format, you will not attempt to modify such portions of the Software, or to reverse engineer or decompile them, except and only to the extent authorized by applicable law. 
3. That the copyright holders (TUM and the authors) are granted back, without any restrictions or limitations, a non-exclusive, perpetual, irrevocable, royalty-free, assignable and sub-licensable license, to reproduce, publicly perform or display, install, use, modify, post, distribute, make and have made, sell and transfer your modifications to and/or derivative works of the Software source code or data, for any purpose.  
4. That any feedback about the Software provided by you to us is voluntarily given, and TUM as well as the authors shall be free to use the feedback as it sees fit without obligation or restriction of any kind, even if the feedback is designated by you as confidential. 

5.   THAT THE SOFTWARE COMES "AS IS", WITH NO WARRANTIES. THIS MEANS NO EXPRESS, IMPLIED OR STATUTORY WARRANTY, INCLUDING WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, ANY WARRANTY AGAINST INTERFERENCE WITH YOUR ENJOYMENT OF THE SOFTWARE OR ANY WARRANTY OF TITLE OR NON-INFRINGEMENT. THERE IS NO WARRANTY THAT THIS SOFTWARE WILL FULFILL ANY OF YOUR PARTICULAR PURPOSES OR NEEDS. ALSO, YOU MUST PASS THIS DISCLAIMER ON WHENEVER YOU DISTRIBUTE THE SOFTWARE OR DERIVATIVE WORKS.
6.	THAT NEITHER TUM NOR ANY AUTHOR OR CONTRIBUTOR TO THE SOFTWARE WILL BE LIABLE FOR ANY DAMAGES RELATED TO THE SOFTWARE OR THIS LICENSE, INCLUDING DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL OR INCIDENTAL DAMAGES, TO THE MAXIMUM EXTENT THE LAW PERMITS, NO MATTER WHAT LEGAL THEORY IT IS BASED ON. ALSO, YOU MUST PASS THIS LIMITATION OF LIABILITY ON WHENEVER YOU DISTRIBUTE THE SOFTWARE OR DERIVATIVE WORKS.
7.	That we have no duty of reasonable care or lack of negligence, and we are not obligated to (and will not) provide technical support for the Software.
8.	That if you breach this license agreement or if you sue anyone over patents that you think may apply to or read on the Software or anyone's use of the Software, this license agreement (and your license and rights obtained herein) terminate automatically.  Upon any such termination, you shall destroy all of your copies of the Software immediately.  Sections 3, 4, 5, 6, 7, 10 and 11 of this license agreement shall survive any termination of this license agreement.
9.	That the patent rights, if any, granted to you in this license agreement only apply to the Software, not to any derivative works you make.
10.	That the Software may be subject to European export or import laws or such laws in other places.  You agree to comply with all such laws and regulations that may apply to the Software after the delivery of the software to you.
11.	That all rights not expressly granted to you in this license agreement are reserved by TUM and the authors.
12.	That this license agreement shall be construed and controlled by the laws of the Federal Republic of Germany, without regard to conflicts of law.  If any provision of this license agreement shall be deemed unenforceable or contrary to law, the rest of this license agreement shall remain in full effect and interpreted in an enforceable manner that most closely captures the intent of the original language. 

 ++ For other, such as commercial, licensing options, please contact the authors ++
 
About openSMILE:
================

openSMILE is a complete and open-source toolkit for audio analysis, processing and classification especially targeted at speech and music applications, e.g. ASR, emotion recognition, or beat tracking and chord detection.
The toolkit is developed at the Institute for Human-Machine Communication at the Technische Universitaet Muenchen in Munich, Germany.
It was started withtin the SEMAINE EU FP7 project.


Third-party dependencies:
=========================

openSMILE uses LibSVM (by Chih-Chung Chang and Chih-Jen Lin) for classification tasks. It is distributed with openSMILE and is included in the src/classifiers/libsvm/ directory.

PortAudio is required for live recording from sound card and for the SEMAINE component.
You can get it from: http://www.portaudio.com
A working snapshot is included in thirdparty/portaudio.tgz

Optionally, openSMILE can be linked against the SEMAINE API and the Julius LVCSR engine, enabling an interface to the SEMAINE system and a keyword spotter component. See http://www.semaine-project.eu/ for details on running the SEMAINE system.


Documentation/Installing/Using:
===============================

openSMILE is well documented in the openSMILE book, which can be found in doc/openSMILE_book.pdf.


Developers:
===========

Incomplete developer's documentation can be found in "doc/developer" and in the openSMILE book.

Information on how to write and compile run-time linkable plug-ins for openSMILE, see the openSMILE book or take a look at the files in the "plugindev" directory, especially the README file.


Getting more help:
==================

If you encounter problems with openSMILE, and solve them yourself, please do inform Florian Eyben via e-mail (lastname at tum.de), so the documentation can be updated!

If you cannot solve the problems yourself, please do also contact Florian Eyben so we can solve the problem together and update the documentation.


