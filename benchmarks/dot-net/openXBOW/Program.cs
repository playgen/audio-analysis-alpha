﻿using System;
using System.Diagnostics;
using openxbow.main;

namespace openXBOW
{
    class Program
    {
        static void Main(string[] args)
        {
            var lld = @"D:\Projects\audio-analysis-dev\benchmarks\input\LLD.csv";
            var outPath = @"D:\Projects\audio-analysis-dev\benchmarks\dot-net\output\BOAW.libsvm";
            var resultPath = @"D:\Projects\audio-analysis-dev\benchmarks\results.log";

            var stopwatch = Stopwatch.StartNew();

            var count = 10;
            for (var i = 0; i < count; i++)
            {
                OpenXBOW.main(new[] { "-i", lld, "-o", outPath, "-a", "10", "-norm", "1", "-b", "codebook"});
            }
            stopwatch.Stop();
            Console.WriteLine(stopwatch.ElapsedMilliseconds);
            Console.WriteLine(stopwatch.ElapsedMilliseconds / count);

            using (var writer = System.IO.File.AppendText(resultPath))
            {
                writer.WriteLine(
                    $"{DateTime.Now}: .NET: openXBOW Runs: {count}, Average: {stopwatch.ElapsedMilliseconds / count} MS, Total: {stopwatch.ElapsedMilliseconds} MS.");
            }

            //Console.ReadKey();
        }
    }
}