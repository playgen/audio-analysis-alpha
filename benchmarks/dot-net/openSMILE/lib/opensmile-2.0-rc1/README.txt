
The documentation can be found in opensmile/doc/openSMILE_book.pdf
Compile on Linux with :
  cd opensmile/
  bash buildStandalone.sh

Main opensmile distribution in directory: opensmile/
Visual Studio binary output (after compilation on Windows): msvcbuild/
Thirdparty (PortAudio, and openCV (which you need to copy there if compiling under Windows)): thirdparty/


